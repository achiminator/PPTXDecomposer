package analze;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import metaModel.Concept;
import metaModel.View;
import pptxHandler.Diagram;
import pptxHandler.Relation;

public class DiagramAnalyzer {

	public void analyze(List<Diagram> pptx, List<View> views) throws FileNotFoundException {
		getStructuralLikelyhood(pptx, views);
		getSemanticalLikelyhood(pptx, views);
	}

	private void getSemanticalLikelyhood(List<Diagram> pptx, List<View> views) throws FileNotFoundException {
		StringBuilder sb = new StringBuilder();
		sb.append("Diagram Name;View Name;Number of Shapes;Reverse Connections;Valid Connections;Not fitted Shapes\n");
		int fullHitCounter = 0, partlyHitCounter = 0;
		for (View view : views) {
			view.clearNullGraphicalRelations();
		}
		for (Diagram diagram : pptx) {
			List<pptxHandler.Shape> fittedShapes = new ArrayList<>();
			List<pptxHandler.Shape> notfittedShapes = new ArrayList<>();
			System.out.println("Diagramtypen:");
			System.out.println(diagram.getDistinctShapeTypes());
			for (View view : views) {
				fullHitCounter = 0;
				partlyHitCounter = 0;
				for (Relation pptRL : diagram.getRelations()) {
					String[] pptxRelation = pptRL.getRelationTypeArray();
					pptxRelation[0] = pptType2ado(pptxRelation[0]);
					pptxRelation[1] = pptType2ado(pptxRelation[1]);
					for (metaModel.Relation metaRL : view.getRelations()) {
						Concept[] metaRelation = metaRL.getRelationTypeArray();
						for (int i = 0; i < metaRelation[0].getGraphtype().size(); i++) {
							for (int j = 0; j < metaRelation[1].getGraphtype().size(); j++) {
								// A "full hit" equals the same starting and ending connector
								if (metaRelation[0].getGraphtype().get(i).equals(pptxRelation[0])
										&& metaRelation[1].getGraphtype().get(j).equals(pptxRelation[1])) {
									fullHitCounter++;
									// Stores all Shapes that are matched
									if (!fittedShapes.contains(pptRL.getStart()))
										fittedShapes.add(pptRL.getStart());
									if (!fittedShapes.contains(pptRL.getEnd()))
										fittedShapes.add(pptRL.getEnd());
									// A "PartlyHit" equals switched start and ending connectors
								}
								if (metaRelation[0].getGraphtype().get(i).equals(pptxRelation[1])
										&& metaRelation[1].getGraphtype().get(j).equals(pptxRelation[0])) {
									partlyHitCounter++;
									if (!fittedShapes.contains(pptRL.getStart()))
										fittedShapes.add(pptRL.getStart());
									if (!fittedShapes.contains(pptRL.getEnd()))
										fittedShapes.add(pptRL.getEnd());
								}
							}
						}
					}

				}
				// Identify the shapes that are NOT connected properly
				for (pptxHandler.Shape shape : diagram.getShapes()) {
					if (!fittedShapes.contains(shape) && !notfittedShapes.contains(shape))
						notfittedShapes.add(shape);
				}
				//Print out the Rows including the shapes that do not fit into the model
				for (pptxHandler.Shape shape : notfittedShapes) {
					sb.append(diagram.getDiagramName() + ";" + view.getName() + ";" + diagram.getShapes().size() + ";"
							+ partlyHitCounter + ";" + fullHitCounter + ";" + shape.getText() + "\n");
				}
			}
		}

		PrintWriter pw = new PrintWriter("SemanticalAnalysis.csv");
		pw.write(sb.toString());
		System.out.println("Semantical Analysis Export:");
		System.out.println(sb.toString());
		pw.close();
	}

	private void getStructuralLikelyhood(List<Diagram> pptx, List<View> meta) throws FileNotFoundException {
		StringBuilder sb = new StringBuilder();
		sb.append("Diagram Name;View Name;shapes not matched by concepts\n");
		for (Diagram diagram : pptx) {
			List<String> comparable = pptType2adolist(diagram.getDistinctShapeTypes());
			System.out.println("ppt: " + diagram.getDistinctShapeTypes());
			System.out.println("pptType2Ado: " + comparable);
			for (View view : meta) {
				List<String> meta_Graphtypes = view.getDistinctConceptTypes();
				System.out.println("Distinct Meta Graphtypes in Meta Model:");
				System.out.println(meta_Graphtypes);
				for (String string : comparable) {
					if (!meta_Graphtypes.contains(string))
						sb.append(diagram.getDiagramName() + ";" + view.getName() + ";" + string + "\n");
				}
				PrintWriter pw = new PrintWriter("StructuralAnalysis.csv");
				pw.write(sb.toString());
				System.out.println("Structural Analysis Export:");
				System.out.println(sb.toString());
				pw.close();
			}
		}
	}

	private List<String> pptType2adolist(List<String> list) {
		List<String> typeList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			typeList.add(pptType2ado(list.get(i)));
		}
		return typeList;
	}

	private String pptType2ado(String list) {
		// System.out.println(list);
		switch (list) {
		case "RECT":
		case "FLOW_CHART_PROCESS":
			list = "RECTANGLE";
			break;
		case "ELLIPSE":
			list = "ELLIPSE";
			break;
		case "ROUND_RECT":
			list = "ROUNDRECT";
			break;
		case "FLOW_CHART_DECISION":
		case "RHOMBUS":
		case "DIAMOND":
			list = "RHOMBUS";
			break;
		case "RT_TRIANGLE":
		case "TRIANGLE":
		case "FLOW_CHART_EXTRACT":
			list = "ISOCENTRIC_TRIANGLE";
			break;
		case "FLOW_CHART_MERGE":
			list = "UPSIDE_ISOCENTRIC_TRIANGLE";
			break;
		case "PARALLELOGRAM":
			list = "PARALLELOGRAM";
			break;
		default:
			list = "POLYGON";
			break;
		}
		return list;
	}
}