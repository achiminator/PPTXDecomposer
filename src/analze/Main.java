package analze;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.xslf.usermodel.XMLSlideShow;

import metaModel.MetaModelHandler;
import metaModel.View;
import pptxHandler.Diagram;
import pptxHandler.DiagramHandler;

public class Main {

	public static void main(String[] args) throws IOException, Exception {
		XMLSlideShow pptx = new XMLSlideShow(new FileInputStream("Slides/Testfiles2.pptx"));
		

		ArrayList<Diagram> diagrams = (ArrayList<Diagram>) DiagramHandler.getDiagrams(pptx);
		System.out.println("ConsolidateDiagrams");
		System.out.println("Origin Diagrams");
		System.out.println(Diagram.printDiagram(diagrams));
		
		System.out.println("Multiple Diagram Problem solved:");
		diagrams = Diagram.cleanMultipleDiagrams(diagrams);
		System.out.println(Diagram.printDiagram(diagrams));
		
		System.out.println("Fractored Diagram Problem solved:");
		diagrams = DiagramHandler.cleanFractoredDiagrams(diagrams);
		System.out.println(Diagram.printDiagram(diagrams));
		MetaModelHandler mmhandler = new MetaModelHandler();
		List<View> views = mmhandler.getMetaModels();

		// End of Diagram-Import

		DiagramAnalyzer da = new DiagramAnalyzer();
		da.analyze(diagrams, views);
	}

}