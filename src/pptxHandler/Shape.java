package pptxHandler;

public class Shape {
	private double positionX;
	private double positionY;
	private double sizeX;
	private double sizeY;
	private final int id;
	private String text;


	private String type;

	public Shape(double positionX,double positionY, double sizeX, double sizeY, String shape, int id) {
		super();
		this.positionX = positionX;
		this.positionY = positionY;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.type = shape;
		this.id = id;
	}
	public Shape(String text, double positionX,double positionY, double sizeX, double sizeY, String shape, int id) {
		super();
		this.text = text;
		this.positionX = positionX;
		this.positionY = positionY;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.type = shape;
		this.id = id;
	}

	public double getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public double getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public double getSizeX() {
		return sizeX;
	}

	public void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}

	public double getSizeY() {
		return sizeY;
	}

	public void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
