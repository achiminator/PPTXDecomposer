package pptxHandler;

public class Relation {
	private Shape start;
	private Shape end;
	

	public Relation(Shape start, Shape end) {
		super();
		this.start = start;
		this.end = end;
	}

	public Shape getStart() {
		return start;
	}

	public void setStart(Shape start) {
		this.start = start;
	}

	public Shape getEnd() {
		return end;
	}
	
	public Shape getCounterPart(Shape shape) {
		if(shape == start)
			return end;
		else if (shape == end)
			return start;
		return null;
	}
	public void setEnd(Shape end) {
		this.end = end;
	}
	public String[] getRelationTypeArray() {
		String[] a = new String[2];
		a[0] = start.getType();
		a[1] = end.getType();
		return a;
	}
}
