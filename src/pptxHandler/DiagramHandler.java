package pptxHandler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.sl.usermodel.PlaceableShape;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFConnectorShape;
import org.apache.poi.xslf.usermodel.XSLFGroupShape;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.apache.xmlbeans.XmlObject;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class DiagramHandler {
	public static List<Diagram> getDiagrams(XMLSlideShow pptx) throws JDOMException, IOException {
		List<XSLFSlide> slides = pptx.getSlides();

		ArrayList<Diagram> diagrams = new ArrayList<Diagram>();
		for (XSLFSlide slide : slides) {
			/*
			 * First of all, all Shapes are stored in the tmpShapeStorage, without
			 * considering whether they are analyzable or not. If they match the right
			 * criterias, the tmp-Storage will be put into the right Classes
			 */
			List<XSLFShape> orignShapes = slide.getShapes();
			DiagramHandler.handleDiagrams(diagrams, orignShapes, "");
		}
		return diagrams;
	}

	public static ArrayList<Diagram> cleanFractoredDiagrams(ArrayList<Diagram> diagrams) {
		ArrayList<Diagram> tmpDiagrams = new ArrayList<>();
		List<Integer> foundedSlides = new ArrayList<>();
		// Without this check, no new Diagram will be added, the result set is 0
		if (diagrams.size() == 1)
			return diagrams;
		for (int i = 0; i < diagrams.size(); i++) {
			// If a slide already has been found as doubled, there is no need to investigate
			// it further
			if (foundedSlides.contains(i))
				continue;
			boolean found = false;
			List<Shape> newShapes = new ArrayList<>();
			Diagram diagram = diagrams.get(i);
			for (Shape shape : diagrams.get(i).getShapes()) {
				if (!newShapes.contains(shape))
					newShapes.add(shape);
				for (int i2 = i + 1; i2 < diagrams.size(); i2++) {
					if (i == i2)
						continue;
					boolean found2 = false;
					List<Shape> newShapes2 = new ArrayList<>();
					for (Shape shape2 : diagrams.get(i2).getShapes()) {
						newShapes2.add(shape2);
						if (shape.getText().equals(shape2.getText()) && shape.getType().equals(shape2.getType())) {
							if (!foundedSlides.contains(i2))
								foundedSlides.add(i2);
							// If a shape is found in another slide as well, it won't be stored as it would
							// be doubled otherwise.
							newShapes2.remove(shape2);
							found = true;
							found2 = true;

						}
					}
					if (found2 == true)
						for (Shape shape2 : newShapes2) {
							if (!findEntry(newShapes, shape2))
								newShapes.add(shape2);
						}

				}
			}
			if (found == true) {
				Diagram tmpDiagram = new Diagram(diagram.getDiagramName());
				tmpDiagram.addShapes(newShapes);
				tmpDiagrams.add(tmpDiagram);

			} else if (found == false)
				tmpDiagrams.add(diagram);
		}
		for (Diagram tmpDiagram : tmpDiagrams) {
			tmpDiagram.createUniqueShapes();
		}
		if (diagrams.size() > tmpDiagrams.size()) {

			tmpDiagrams = cleanFractoredDiagrams(tmpDiagrams);
		}
		return tmpDiagrams;

	}

	private static boolean findEntry(List<Shape> shapes, Shape checkShape) {
		boolean found = false;
		for (Shape shape : shapes) {
			if (shape.getText().equals(checkShape.getText()) && shape.getType().equals(checkShape.getType()))
				found = true;
		}
		return found;
	}

	public static ArrayList<Diagram> handleDiagrams(ArrayList<Diagram> diagrams, List<XSLFShape> orignShape,
			String slideTitle) throws JDOMException, IOException {

		ArrayList<XSLFShape> tmpShapeStorage = new ArrayList<XSLFShape>();
		boolean slideConsidered = false;

		for (XSLFShape sh : orignShape) {
			tmpShapeStorage.add(sh);

			// shapes's anchor which defines the position of this shape in the slide
			if (sh instanceof PlaceableShape) {
				if (sh instanceof XSLFGroupShape) {
					XSLFGroupShape shape = (XSLFGroupShape) sh;
					handleDiagrams(diagrams, shape.getShapes(), "Group-Shape");
					//diagrams.addAll(handleDiagrams(diagrams, shape.getShapes(), "Group-Shape"));
				}
			}
			if (sh instanceof XSLFConnectorShape)
				slideConsidered = lineHasConnector(sh);
		}
		if (slideConsidered == true)
			diagrams.add(storeDiagram(tmpShapeStorage, "Group-Shape"));
		slideConsidered = false;
		return (diagrams);
	}
	/*
	 * The whole slideTitle Solution is a very badly and urgent fix a grouping
	 * problem. Before that, grouping items were stored in a seperatly item with the
	 * Title "No Title Assigned". The slideTitle attribute at least gives the
	 * heading "Group-Shape". Please change that as soon as you work on grouping, it
	 * is otherwise not needed at all.
	 */

	protected static Diagram storeDiagram(ArrayList<XSLFShape> slide, String slideTitle)
			throws JDOMException, IOException {
		Diagram diagram = new Diagram("No Title Assigned");
		// First of all, the shapes are stored in the new Class Diagram with all
		// Attributes that belong to the shapes
		for (XSLFShape sh : slide) {
			if (sh instanceof XSLFTextShape) {
				XSLFTextShape shape = (XSLFTextShape) sh;
				// shapes's anchor which defines the position of this shape in the slide
				try {
					// TODO: Das try-catch ist noch ein wenig unsauber
					if (shape.getShapeType() != null) {
						diagram.addshape(shape.getAnchor().getX(), shape.getAnchor().getY(),
								shape.getAnchor().getWidth(), shape.getAnchor().getHeight(),
								shape.getShapeType().name(), shape.getShapeId());
						diagram.addShapeTextById(shape.getShapeId(), shape.getText());
					} else {
						// Set Title if Diagram Title if Field is Type "Title"
						if (shape.getTextType().name() == "TITLE") {
							slideTitle = "";
							diagram.setDiagramName(shape.getText());
						}
					}
				} catch (Throwable e) {
				}
				if (slideTitle != "")
					diagram.setDiagramName(slideTitle);
			}
		}
		// In the next step, the Shapes are getting connected with each other, whenever
		// there is a Connector Shape that binds these.
		for (XSLFShape sh : slide) {
			if (sh instanceof XSLFConnectorShape) {
				XSLFConnectorShape line = (XSLFConnectorShape) sh;
				Element lineXML = getLineXML(line);
				int idStart = -1;
				int idEnd = -1;
				try {
					idStart = getStartConnectorID(lineXML);
					idEnd = getEndConnectorID(lineXML);
					diagram.addRelation(idStart, idEnd);
				} catch (Exception e) {
					break;
				}
			}
		}
		// Now that the relations between objects are set, there is no need to store the
		// Relations as shapes anymore. All Connector shapes are therefore dropped out
		// of the diagram class.
		for (XSLFShape sh : slide) {
			if (sh instanceof XSLFConnectorShape)
				diagram.deleteShape(sh.getShapeId());
		}

		return diagram;
	}

	protected static boolean lineHasConnector(XSLFShape sh) throws JDOMException, IOException {
		boolean slideConsidered;
		slideConsidered = true;
		XSLFConnectorShape line = (XSLFConnectorShape) sh;
		Element lineXML = getLineXML(line);
		try {
			getStartConnectorID(lineXML);
			getEndConnectorID(lineXML);
			slideConsidered = true;
		} catch (Exception e) {
			slideConsidered = false;
		}
		return slideConsidered;
	}

	private static int getStartConnectorID(Element lineXML) {
		Namespace ns_a = lineXML.getNamespace("a");
		Namespace ns_p = lineXML.getNamespace("p");
		// Traverse the XML-Graph to the Connector Elements
		Element connectors = lineXML.getChild("nvCxnSpPr", ns_p).getChild("cNvCxnSpPr", ns_p);
		String id = connectors.getChild("stCxn", ns_a).getAttribute("id").getValue();
		return (Integer.parseInt(id));
	}

	private static int getEndConnectorID(Element lineXML) {
		Namespace ns_a = lineXML.getNamespace("a");
		Namespace ns_p = lineXML.getNamespace("p");
		// Traverse the XML-Graph to the Connector Elements
		Element connectors = lineXML.getChild("nvCxnSpPr", ns_p).getChild("cNvCxnSpPr", ns_p);
		String id = connectors.getChild("endCxn", ns_a).getAttribute("id").getValue();
		return (Integer.parseInt(id));
	}

	private static Element getLineXML(XSLFConnectorShape line) throws JDOMException, IOException {
		XmlObject xml = line.getXmlObject();
		SAXBuilder saxBuilder = new SAXBuilder();
		Document document = (Document) saxBuilder.build(new ByteArrayInputStream(xml.toString().getBytes()));
		Element classElement = (Element) document.getRootElement();
		return classElement;
	}

}
