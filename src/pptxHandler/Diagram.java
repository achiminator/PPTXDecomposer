package pptxHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.plaf.synth.SynthSpinnerUI;

import org.apache.commons.collections4.functors.ForClosure;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.apache.poi.ss.formula.FormulaRenderingWorkbook;

/**
 * @author achim
 *
 */
public class Diagram {
	private String diagramName;
	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	int[] relation;
	private ArrayList<Relation> relations = new ArrayList<Relation>();

	public Diagram(String diagramName) {
		super();
		this.diagramName = diagramName;
	}

	public void addshape(double positionX, double positionY, double sizeX, double sizeY, String shape, int id) {
		shapes.add(new Shape(positionX, positionY, sizeX, sizeY, shape, id));
	}

	public void addshape(Shape shape) {
		shapes.add(shape);
	}

	public void addShapeTextById(int id, String text) {
		this.getShapeById(id).setText(text);
	}

	public Shape getShapeById(int id) {
		for (Shape sh : shapes) {
			if (sh.getId() == id)
				return sh;
		}
		return null;
	}

	public ArrayList<Shape> getRelationPartners(Shape shape) {
		ArrayList<Shape> rlPartners = new ArrayList<Shape>();
		for (Relation rl : relations) {
			if (shape == rl.getStart()) {
				rlPartners.add(rl.getEnd());
			} else if (shape == rl.getEnd())
				rlPartners.add(rl.getStart());
		}
		return (rlPartners);
	}

	public static String printDiagram(ArrayList<Diagram> diagrams) {
		StringBuilder sb = new StringBuilder();
		sb.append("Diagramname;ShapeType;ShapeText");
		for (Diagram diagram : diagrams) {
			for (Shape shape : diagram.getShapes()) {
				sb.append("\n");
				sb.append(diagram.getDiagramName());
				sb.append(";");
				sb.append(shape.getType());
				sb.append(";");
				sb.append(shape.getText());
				
			}
		}
		return sb.toString();
	}

	public void addRelation(int idStart, int idEnd) {
		this.relations.add(new Relation(getShapeById(idStart), getShapeById(idEnd)));
	}

	public void addShapes(List<Shape> shapes) {
		this.shapes.addAll(shapes);
	}

	public void deleteShape(int id) {
		this.shapes.remove(this.getShapeById(id));
	}

	public String getDiagramName() {
		return diagramName;
	}

	public void setDiagramName(String diagramName) {
		this.diagramName = diagramName;
	}

	public ArrayList<Shape> getShapes() {
		return shapes;
	}

	// Filters the Shapes for same name and type and deletes those who are doubled
	// with a change of the corresponding relations. This cleans the shape array.
	public void createUniqueShapes() {
		List<Shape> shapesToDelete = new ArrayList<>();
		for (int i = 0; i < shapes.size(); i++) {
			for (int j = i + 1; j < shapes.size(); j++) {
				if (shapes.get(i).getText().equals(shapes.get(j).getText())
						&& shapes.get(i).getType().equals(shapes.get(j).getType())) {
					shapesToDelete.add(shapes.get(j));
					for (Relation relation : getConnectedRelation(shapes.get(j))) {
						if (relation.getEnd() == shapes.get(j))
							relations.add(new Relation(relation.getStart(), shapes.get(i)));
						else if (relation.getStart() == shapes.get(j))
							relations.add(new Relation(shapes.get(i), relation.getEnd()));
						relations.remove(relation);
					}
				}
			}
		}
		shapes.removeAll(shapesToDelete);
	}

	public List<Relation> getConnectedRelation(Shape shape) {
		List<Relation> relations = new ArrayList<>();
		for (Relation relation : this.relations) {
			if (relation.getStart() == shape || relation.getEnd() == shape)
				relations.add(relation);
		}
		return relations;

	}

	// Identifies multiple Diagrams in one diagram fragment if there is no
	// relation at all between group of shapes. It then expands the list of diagrams
	// towards the new diagram.
	public static ArrayList<Diagram> cleanMultipleDiagrams(ArrayList<Diagram> diagrams) {
		List<Diagram> newDiagrams = new ArrayList<>();
		for (Diagram diagram : diagrams) {

			Map<Shape, Integer> map = new HashMap<Shape, Integer>();
			int counter = 0;
			for (Relation relation : diagram.relations) {
				if (map.containsKey(relation.getStart()) && !map.containsKey(relation.getEnd()))
					map.put(relation.getEnd(), map.get(relation.getStart()));
				else if (!map.containsKey(relation.getStart()) && map.containsKey(relation.getEnd()))
					map.put(relation.getStart(), map.get(relation.getEnd()));
				else if (!map.containsKey(relation.getStart()) && !map.containsKey(relation.getEnd())) {
					counter++;
					map.put(relation.getStart(), counter);
					map.put(relation.getEnd(), counter);

				}
			}
			map = restructureMap(map, diagram.relations);
			Map<Integer, Diagram> diagramMap = new HashMap<Integer, Diagram>();
			for (Shape shape : map.keySet()) {
				if (map.get(shape) > 1) {
					int i = map.get(shape);
					if (!diagramMap.containsKey(i)) {
						diagramMap.put(i, new Diagram(diagram.diagramName + " - " + i));
					}
					diagramMap.get(i).addshape(shape);
				}
			}
			for (Diagram diagram2 : diagramMap.values()) {
				for (Shape shape : diagram2.getShapes()) {
					for (Relation relation : diagram.getRelationsbyShape(shape)) {
						if (!diagram2.getRelations().contains(relation))
							diagram2.addRelation(relation);
					}
				}
				newDiagrams.add(diagram2);
			}
		}
		for (Diagram diagram : diagrams) {
			for (Diagram newDiagram : newDiagrams) {
				for (Shape shape : newDiagram.getShapes()) {
					diagram.shapes.remove(shape);
				}
			}
		}
		diagrams.addAll(newDiagrams);
		return diagrams;

	}

	private void addRelation(Relation relation) {
		relations.add(relation);
	}

	public List<Relation> getRelationsbyShape(Shape shape) {
		List<Relation> newRelations = new ArrayList<>();
		for (Relation relation : this.relations) {
			if (relation.getStart() == shape || relation.getEnd() == shape)
				newRelations.add(relation);
		}
		return newRelations;
	}

	private static Map<Shape, Integer> restructureMap(Map<Shape, Integer> map, List<Relation> relations) {
		Map<Shape, Integer> tmpMap = new HashMap<Shape, Integer>();
		for (Shape shape : map.keySet()) {
			for (Shape shape2 : searchCounterPart(shape, relations)) {
				if (map.get(shape) < map.get(shape2))
					tmpMap.put(shape2, map.get(shape));
				else if (map.get(shape) > map.get(shape2))
					tmpMap.put(shape, map.get(shape2));

			}
		}
		for (Shape shape : tmpMap.keySet()) {
			map.put(shape, tmpMap.get(shape));
		}
		if (tmpMap.size() > 0)
			map = restructureMap(map, relations);
		return map;
	}

	private static List<Shape> searchCounterPart(Shape shape, List<Relation> relations) {
		List<Shape> shapes = new ArrayList<>();
		for (Relation relation : relations) {
			if (relation.getCounterPart(shape) != null)
				shapes.add(relation.getCounterPart(shape));
		}
		return shapes;
	}

	public ArrayList<String> getDistinctShapeTypes() {
		ArrayList<String> list = new ArrayList<>();
		for (Shape shape : shapes) {
			if (!list.contains(shape.getType()))
				list.add(shape.getType());
		}
		return list;
	}

	public ArrayList<Relation> getRelations() {
		return relations;
	}

}
