package metaModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MetaModelHandler {

	public List<View> getMetaModels() throws ParserConfigurationException, SAXException, IOException,
			FileNotFoundException, XPathExpressionException {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//		Document doc = builder.parse(new FileInputStream("er3.xml"));
		Document doc = builder.parse(new FileInputStream("EPC.xml"));
		doc.getDocumentElement().normalize();
		XPathFactory xpathfactory = XPathFactory.newInstance();
		XPath xpath = xpathfactory.newXPath();
		// Identify contained views 
		String expression = "/library/attributes/attribute[@name=\"Modi\"]/value/leo/modeltype/@val";
		NodeList nl = (NodeList) xpath.compile(expression).evaluate(doc, XPathConstants.NODESET);
		List<View> views = new ArrayList<View>();
		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			if (node != null)
				views.add(new View(node.getTextContent()));
		}
		// Fill the Views with the corresponding Concepts
		expression = "/library/attributes/attribute[@name=\"Modi\"]/value/leo/*/@val";
		nl = (NodeList) xpath.compile(expression).evaluate(doc, XPathConstants.NODESET);
		int idView = -1;
		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			String txt = node.getTextContent();
			if (View.getViewByName(views, txt) != null)
				idView++;
			else if (idView == -1)
				continue;
			else
				views.get(idView).addConcept(txt);
		}
		// Get Shapetype of Class

		// Problem: a Concept might not be represented by only one graphical
		// representation, but by more. To resolve that, a List of graphical
		// representations exists in the Concept-Class
		for (View view : views) {
			for (Concept cm : view.getConcepts()) {
				expression = "/library/classes/class[@name=\"" + cm.getName()
						+ "\"]/attributes/classattribute[@name=\"GraphRep\"]/value/leo/child::*";
				// cm.setGraphtype(graphtype);
				System.out.println(expression);
				nl = (NodeList) xpath.compile(expression).evaluate(doc, XPathConstants.NODESET);
				for (int i = 0; i < nl.getLength(); i++) {
					String nodeValue = nl.item(i).getNodeName();
					switch (nodeValue) {
					case "ellipse":
						cm.addUniqueGraphtype("ELLIPSE");
						break;
					case "rectangle":
						cm.addUniqueGraphtype("RECTANGLE");
						break;
					case "roundrect":
						cm.addUniqueGraphtype("ROUNDRECT");
						break;
					case "polygon":
						// All that is not represented by an ellipse, rectangle, round rectangle or a
						// Pie is represented by a Polygon (e.g. Rhombus, Triangle). This part extracts
						// the information out of the XML-library with X-PATH and converts graphical
						// representations to forms
						expression = "/library/classes/class[@name=\"" + cm.getName()
								+ "\"]/attributes/classattribute[@name=\"GraphRep\"]/value/leo/polygon/attribute::*";
						System.out.println(expression);
						NodeList grapghAttributes = (NodeList) xpath.compile(expression).evaluate(doc,
								XPathConstants.NODESET);
						if (grapghAttributes.item(0) != null) {
							ArrayList<Double> x = new ArrayList<>();
							ArrayList<Double> y = new ArrayList<>();
							ArrayList<Integer> x_found = new ArrayList<>();
							ArrayList<Integer> y_found = new ArrayList<>();
							/*
							 * The return value contains at first the counter of attributes (how many x and
							 * y values), then the attributes.
							 */
							for (int j = 1; j < grapghAttributes.getLength(); j++) {

								String result;
								result = grapghAttributes.item(j).getNodeValue();
								if (grapghAttributes.item(j).getNodeName().contains("x")) {
									if (result.contains("cm")) {

										x_found.add(Integer.parseInt(
												grapghAttributes.item(j).getNodeName().replaceAll("[^?0-9]+", "")));

										result = result.replaceAll("cm", "");
										double position = 0;

										try {
											position = Double.parseDouble(result);
										} catch (Exception e) {
											// TODO: handle finally clause

										}
										x.add(position);

									}
								}
								if (grapghAttributes.item(j).getNodeName().contains("y")) {
									if (result.contains("cm")) {
										// result = result.replaceAll("[^-?0-9]+", "");
										y_found.add(Integer.parseInt(
												grapghAttributes.item(j).getNodeName().replaceAll("[^?0-9]+", "")));
										result = result.replaceAll("cm", "");
										double position = 0;
										try {
											position = Double.parseDouble(result);
										} catch (Exception e) {
										}
										y.add(position);
									}
								}
								// Die XPath gibt alle Polygon-M�glichkeiten nacheinander an, jedes beginnt mit
								// dem Node-Namen "val". Sofern dies zutrifft, oder allerdings das letzte
								// Element der Liste auftritt, wird auf die Form des Objekts �berp�ft.
								if ((grapghAttributes.item(j).getNodeName() == "val"
										|| j + 1 == grapghAttributes.getLength()) && x.size() > 0 && y.size() > 0) {

									// Graphical Representations contain X,Y-Coordinates, but a 0 does not have to
									// be written down. This part addes 0 to the Array where they are needed (and
									// not represented in the XML)
									for (int k = 0; k < x_found.size(); k++) {
										if (x_found.get(k) > y_found.get(k)) {
											x.add(k, (double) 0);
											x_found.add(k, y_found.get(k));
										}

										else if (x_found.get(k) < y_found.get(k)) {
											y.add(k, (double) 0);
											y_found.add(k, x_found.get(k));
										}
										if (k + 1 == x_found.size() && y_found.size() > x_found.size())
											x.add(k + 1, (double) 0);
									}
									System.out.println(x);
									System.out.println(y);
									// Detecting Rhombus or Parallelogramm
									if (x.size() == 4 && y.size() == 4) {
										if (x.get(0) == -1 * x.get(2) && y.get(1) == -1 * y.get(3))
											cm.addUniqueGraphtype("RHOMBUS");
										else if (x.get(0) == -1 * x.get(2) && x.get(1) == -1 * x.get(3)
												&& y.get(0) == y.get(1) && y.get(2) == y.get(3))
											cm.addUniqueGraphtype("PARALLELOGRAM");
									}
									// Detecting Triangles
									else if (x.size() == 3 && y.size() == 3) {
										//
										if (x.get(0) == -1 * x.get(2) && x.get(1) == x.get(2) + x.get(0)
												&& y.get(2) < 0)
											cm.addUniqueGraphtype("ISOCENTRIC_TRIANGLE");
										else if (x.get(0) == -1 * x.get(2) && x.get(1) == x.get(2) + x.get(0)
												&& y.get(2) > 0)
											cm.addUniqueGraphtype("UPSIDE_ISOCENTRIC_TRIANGLE");
									} else
										cm.addUniqueGraphtype("POLYGON");

									// Reset Graphical ArrayLists (X-Y Coordinates of Polygon)
									x = new ArrayList<>();
									y = new ArrayList<>();
									x_found = new ArrayList<>();
									y_found = new ArrayList<>();
								}
							}
							break;
						}
						System.out.println("Found Graphtype in MetaModel" + cm.getGraphtype());
					}
				}
			}
		}
		// Identify the Relationtypes

		for (View view : views) {
			// Check if Concept is a Relation

			// This attribute stores the Concepts that are not concepts but Relations
			List<Concept> relationConcepts = new ArrayList<>();
			for (Concept cm : view.getConcepts()) {
				List<String> startRelations = new ArrayList<>();
				List<String> endRelations = new ArrayList<>();
				expression = "/library/classes/relationclass[@name=\"" + cm.getName() + "\"]";
				Node node = (Node) xpath.compile(expression).evaluate(doc, XPathConstants.NODE);
				// if so, check start and end-points
				if (node != null) {
					{
						expression = "/library/classes/relationclass[@name=\"" + cm.getName() + "\"]/@fromclass";
						Node innerNode = (Node) xpath.compile(expression).evaluate(doc, XPathConstants.NODE);
						startRelations = getSubClass(startRelations, doc, xpath, innerNode);
					}
					{
						expression = "/library/classes/relationclass[@name=\"" + cm.getName() + "\"]/@toclass";
						Node innerNode = (Node) xpath.compile(expression).evaluate(doc, XPathConstants.NODE);
						endRelations = getSubClass(endRelations, doc, xpath, innerNode);
					}
					// Adds the lists of start and endConcepts to the structured Relation-Attribute
					for (String startConceptName : startRelations) {
						for (String endConceptName : endRelations) {
							Concept startConcept = Concept.getConceptByName(startConceptName, view.getConcepts());
							Concept endConcept = Concept.getConceptByName(endConceptName, view.getConcepts());
							if ((startConcept != null) && (endConcept != null))
								view.addRelation(startConcept, endConcept);
						}
					}
					// As it apperead that the stored Concept is a Relation, this relation will be
					// stored within the relationattribute, not the concept attribute.
					relationConcepts.add(cm);
				}
			}
			view.deleteConcepts(relationConcepts);
		}
		System.out.println("Metamodel analyzed");
		return views;
	}

	private static List<String> getSubClass(List<String> results, Document doc, XPath xpath, Node startNode)
			throws XPathExpressionException {
		String expression;
		NodeList nl;
		List<String> tmpResult = new ArrayList<>();
		expression = "/library/classes/class[@superclass=\"" + startNode.getNodeValue() + "\"]/@name";
		nl = (NodeList) xpath.compile(expression).evaluate(doc, XPathConstants.NODESET);
		if (nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {
				Node node = nl.item(i);
				tmpResult = getSubClass(tmpResult, doc, xpath, node);
			}
		} else {
			tmpResult.add(startNode.getNodeValue());
		}
		results.addAll(tmpResult);
		return results;
	}
}