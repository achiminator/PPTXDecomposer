package metaModel;

import java.util.ArrayList;
import java.util.List;

public class View {
	private String name;
	private List<Concept> concepts = new ArrayList<Concept>();

	private List<Relation> relations = new ArrayList<>();

	public View(String name) {
		super();
		this.name = name;
	}

	public void addConcept(String name) {
		concepts.add(new Concept(name));
	}

	public void addConcept(Concept cm) {
		concepts.add(cm);
	}

	public void addConcept(List<Concept> cm) {
		concepts.addAll(cm);
	}

	public static View getViewByName(List<View> views, String viewName) {
		for (View view : views) {
			if (view.getName() == viewName)
				return (view);
		}
		return null;
	}

	public String getName() {
		return (this.name);
	}

	public List<Concept> getConcepts() {
		return concepts;
	}

	public void addRelation(Concept start, Concept end) {
		relations.add(new Relation(start, end));
	}

	public void setRelation(List<Relation> relations) {
		this.relations = relations;
	}

	public List<Relation> getRelations() {
		return relations;
	}

	public void clearNullGraphicalRelations() {
		// sometimes the graphical representation to Objects are not available. This
		// clears these objects from the realation list
		List<Relation> list = new ArrayList<>();
		for (Relation relation : relations) {
			if (relation.getStart().getGraphtype() != null && relation.getEnd().getGraphtype() != null)
				list.add(relation);
		}
		relations = list;
	}

	public void deleteConcept(Concept cm) {
		concepts.remove(cm);
	}

	public List<String> getDistinctConceptTypes() {
		List<String> list = new ArrayList<>();
		for (Concept concept : concepts) {
			for (String string : concept.getGraphtype()) {
				if (!list.contains(string))
					list.add(string);

			}
		}
		return list;
	}

	public void deleteConcepts(List<Concept> relationConcepts) {
		for (Concept concept : relationConcepts) {
			concepts.remove(concept);
		}
	}

}
