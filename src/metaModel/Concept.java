package metaModel;

import java.util.ArrayList;
import java.util.List;

public class Concept {
	private String name;
	private List<String> graphtypes;

	public Concept(String name) {
		super();
		this.name = name;
		graphtypes = new ArrayList<>();
	}

	public void addUniqueGraphtype(String type) {
		if (!graphtypes.contains(type))
			graphtypes.add(type);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getGraphtype() {
		return graphtypes;
	}

	public static Concept getConceptByName(String name, List<Concept> concepts) {
		for (Concept concept : concepts) {
			if (concept.getName().equals(name))
				return concept;
		}

		return null;
	}

}
