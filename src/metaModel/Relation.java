package metaModel;

public class Relation {
	private Concept start;
	private Concept end;

	public Relation(Concept start, Concept end) {
		super();
		this.start = start;
		this.end = end;
	}

	public Concept getStart() {
		return start;
	}

	public void setStart(Concept start) {
		this.start = start;
	}

	public Concept getEnd() {
		return end;
	}
//	public String[][] getRelationTypeArray() {
//		String[][] a = new String[2][];
//		a[0] = new String[start.getGraphtype().size()];
//		a[1] = new String[end.getGraphtype().size()];
//		for (int i = 0; i < start.getGraphtype().size(); i++) {
//			a[0][i] = start.getGraphtype().get(i);
//		}
//		for (int i = 0; i < end.getGraphtype().size(); i++) {
//			a[1][i] = end.getGraphtype().get(i);
//		}
//		return a;
//	}

	public Concept[] getRelationTypeArray() {
		Concept[]a = new Concept[2];
		a[0] = start;
		a[1] = end;
		return a;
	}
	
	public void setEnd(Concept end) {
		this.end = end;
	}
}
